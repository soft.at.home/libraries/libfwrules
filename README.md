# libfwrules

[TOC]

# Introduction

libfwrules is a library for storing firewall rules in memory before processing
them. It acts as an intermediate between multiple data model formats.

Libfwrules can be read as "lib firewall rules".

Suppose you are writing an application that interacts with, for example,
netfilter. The application has to support multiple data models, like TR-181,
TR-069, XML, or any other data model out there.

In general, you have to convert the upper layer data model parameters to the
lower layer netfilter ones. This can be handled in two ways:

## Using a direct approach

In the direct approach, you love writing lots of code doing the same thing
at the end. For every data model, a translation logic part is written,
validating the data model parameters and passing them to the netfilter
translation logic.

The latter part converts the data model to netfilter API parameters, suitable
to be passed to the Linux kernel.

This approach is good if you want to support one data model only. When
multiple data models are required, several disadvantages rise:

- boilerplate code: for each data model, the netfilter translation logic must be re-written.
- shareable: the netfilter translation logic exists in the application only. Other applications
cannot make use of it.

![Without libfwrules](doc/images/overview_no_libfwrules.svg)

## Using libfwrules

To counter the direct approach disadvantages, libfwrules comes into place.
This library can be put in between the data model's translation logic and the
netfilter translation logic. That way, the data model is converted to an
intermediate format, provided by libfwrules.

In the upper layer, a translation logic must exist for each data model. This is
the part that queries the data model parameters, validates them and creates a
new firewall rule using the libfwrules API.

In the lower layer, only one implementation of netfilter translation logic must
be written, since the intermediate format is now used as the input for that
logic.

Further, this library can of course be shared between different applications.

![With libfwrules](doc/images/overview.svg)

# Overview

libfwrules is built on top of the <code>Ambiorix</code> framework. Please consult
the <code>Ambiorix</code> documentation for more information about the used data
types.

The next image gives an overview of the library and its entities.

![Library overview](doc/images/libfwrules.svg)

libfwrules consists of several entities:

## Rule
A rule is the smallest entity to act on and represents a single firewall rule.<br />
Data type: <code>fw_rule_t</code>

## Folder
A folder is a group of rules, belonging together.<br />
Data type: <code>fw_folder_t</code>

## g_rules
An internal global linked list of type <code>amxc_llist_t</code>. This list is used to
store all rules. It doesn't matter if a rule belongs to a folder or not.

## fw_commit
This is actually a function, used to commit all rules. When this function is
called, each rule's flag (<code>fw_rule_flag_t</code>) in <code>g_rules</code> is checked.
Depending on the flag, the rule is added, modified or deleted. A callback
function can be provided as argument.

## Callback
<code>fw_commit()</code> will invoke the callback function, which resides in
the userspace application. The callback function should add, modify or delete a
firewall rule in the system.

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/libraries/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>Libfwrules</code> depends on the following libraries:

- libamxc
- libsahtrace

These libraries can be installed in the container:

    sudo apt-get install libamxc mod-sahtrace sah-lib-sahtrace-dev iproute2

The `iproute2`is installed for later purposes and provides the `ip` command.

### Build libfwrules

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/libraries/
        git clone git@gitlab.com:prpl-foundation/components/core/libraries/libfwrules.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/libraries/libfwrules
        make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/libraries/libfwrules
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/libraries/libfwrules
    make package

The packages generated are:

    ~/amx/libraries/libfwrules/libfwrules-<VERSION>.tar.gz
    ~/amx/libraries/libfwrules/libfwrules-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/libraries/libfwrules/libfwrules-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `libfwrules`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/libraries/libfwrules/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/libraries/libfwrules/tests
    make run coverage

You can combine both commands:

    cd ~/amx/libraries/libfwrules
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/libraries/libfwrules/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/libraries/libfwrules/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/libraries/libfwrules$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/libraries/libfwrules/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/libraries/libfwrules

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.
- docs directory: contains the Doxygen settings, code examples and additional pages used in Doxygen.

The code itself is documented in the approriate header and source files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/libraries/libfwrules
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/libraries/libfwrules/output/html/index.html

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/libraries/libfwrules$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/libraries/libfwrules/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the API, code
examples and to unveal some secrets about folders, rules and much more.

