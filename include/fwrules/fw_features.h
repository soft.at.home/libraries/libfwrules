/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_FEATURES_H__)
#define __FW_FEATURES_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

/**
   @defgroup feature Feature API
   @brief
   The features that can be added to a folder.

   @details
   A folder (@ref fw_folder_t) can have multiple rules, having common
   parameters, like a table, chain, ... These parameters are set in the default rule.

   To create a more specific rule, that does not share a parameter with the
   default rule, a feature rule is created.
 */

/**
   @ingroup feature
   @brief
   Possible features for a folder.
 */
typedef enum _fw_feature {
    FW_FEATURE_IN_INTERFACE,        /**< Set the ingress interface. */
    FW_FEATURE_DESTINATION,         /**< Set the destination IP address. */
    FW_FEATURE_DESTINATION_PORT,    /**< Set the destination port number. */
    FW_FEATURE_PROTOCOL,            /**< Set the IP protocol. See /etc/protocols for more info. */
    FW_FEATURE_SOURCE,              /**< Set the source IP address. */
    FW_FEATURE_SOURCE_MAC,          /**< Set the source MAC address. */
    FW_FEATURE_WHITELIST,           /**< Allow an entity to pass through the firewall. */
    FW_FEATURE_SOURCE_PORT,         /**< Set the source port number. */
    FW_FEATURE_TARGET,              /**< Set the jump target, e.g. dscp, nfqueue, mark, classify.. */
    FW_FEATURE_DSCP,                /**< Used to set one or more DSCP values, often used for QoS. */
    FW_FEATURE_IPVERSION,           /**< Can be used to set a specific IPv4 or IPv6 rule. */
    FW_FEATURE_TABLE,               /**< Set a different table than the default rule. */
    FW_FEATURE_DNAT,                /**< Used to set one or more destination NAT rules. */
    FW_FEATURE_SNAT,                /**< Used to set one or more source NAT rules. */
    FW_FEATURE_POLICY,              /**< Set an ACCEPT, REJECT or DROP policy. */
    FW_FEATURE_SPOOFING,            /**< To block traffic from a particular source to destination. */
    FW_FEATURE_ISOLATION,           /**< To block traffic from an ingress interface to a destination. */
    FW_FEATURE_OUT_INTERFACE,       /**< Set the egress interface. */
    FW_FEATURE_STRING,              /**< Set a pattern to match with. */
    FW_FEATURE_TIME,                /**< Set a time to match with. */
    FW_FEATURE_PHYSDEV_IN,          /**< Set an input bridge port. */
    FW_FEATURE_PHYSDEV_OUT,         /**< Set an output bridge port. */
    FW_FEATURE_SOURCE_IPSET,        /**< Set a source ipset to match with. */
    FW_FEATURE_DESTINATION_IPSET,   /**< Set a destination ipset to match with. */
    FW_FEATURE_LAST                 /**< Last item. Do not use. */
} fw_feature_t;

/**
   @ingroup feature
   @brief
   Check if feature is valid.

   @details
   @param[in] feature The feature to be checked.
   @returns @c true if the feature is valid, otherwise @c false.
 */
bool fw_feature_is_valid(const fw_feature_t feature);

/**
   @ingroup feature
   @brief
   Check if @c features contain feature flag @c feature.

   @param[in] features The feature set in a @ref folder.
   @param[in] feature The feature flag that @c features must contain.

   @returns @c true if the feature is present, otherwise @c false.
 */
bool fw_feature_contains(const int features, const fw_feature_t feature);

/**
   @ingroup feature
   @brief
   Set a feature flag.

   @param[in] features A pointer to the feature set.
   @param[in] feature The feature to be set.

   @returns @c 0 if successful, otherwise @c -1.
 */
int fw_feature_set(int* features, const fw_feature_t feature);

/**
   @ingroup feature
   @brief
   Unset a feature flag.

   @param[in] features A pointer to the feature set.
   @param[in] feature The feature to be unset.

   @returns @c 0 if successful, otherwise @c -1.
 */
int fw_feature_unset(int* features, const fw_feature_t feature);

#ifdef __cplusplus
}
#endif

#endif // __FW_FEATURES_H
