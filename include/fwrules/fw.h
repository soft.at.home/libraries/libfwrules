/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_H__)
#define __FW_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

#include <fwrules/fw_rule.h>

/**
   @ingroup folder
   @brief
   Callback function the application must implement to process firewall rules in a folder.

   The callback function is used to actually process a firewall rule. For each
   rule in a folder, the callback function is called when a rule is new,
   modified or deleted. It is up to the application to do something with the
   rule. E.g., the application can add, modify or delete the rule to/from the
   underlying firewall.

   The callback is invoked when @ref fw_commit is called.

   @note
   - The callback function is not called for new or modified rules if they are
   not enabled. Use @ref fw_folder_set_enabled.
   - The callback function is always called when a rule is marked for deletion.

   @param[in] rule Pointer to a rule that must be processed.
   @param[in] flag The state of a rule. This can be new, modified or deleted.
   @param[in] chain The rule's chain. Can also be retrieved by @ref
   fw_rule_get_chain.
   @param[in] table The rule's table. Can also be retrieved by @ref
   fw_rule_get_table.
   @param[in] index The index for this rule's table-chain combination.

   @returns
   @c true when the application handled the rule successfully, otherwise @c false.
 */
typedef bool (* fw_callback_t)(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                               const char* chain, const char* table, int index);

/**
   @ingroup folder
   @brief
   Commit the rules in a folder.

   When all rules are present in a folder, it's time to commit them to the
   system. This function loops over the global rules list (@ref g_rules). For each
   valid rule, the callback function @c cb is called.

   A rule is only valid if it has a table and chain set.

   The callback function is called when:
   - A rule is valid (table and chain are set).
   - A rule is new and enabled. This triggers a @ref FW_RULE_FLAG_NEW event.
   - A rule is modifed. This triggers a @ref FW_RULE_FLAG_DELETED event first,
   followed by a @ref FW_RULE_FLAG_MODIFIED event. The latter one is only
   triggered when the rule is enabled.
   - A rule is deleted. This triggers a @ref FW_RULE_FLAG_DELETED event.

   @attention
   This function should be invoked after:
   - Default and feature rules are all set.
   - @ref fw_folder_delete_rules.
   - @ref fw_folder_delete_feature_rules.
   - @ref fw_folder_delete.

   @param[in] cb Callback function to invoke.

   @returns
   @returns @c 0 on success, @c -1 on failure, @c -2 on failure of the callback function.
 */
int fw_commit(fw_callback_t cb);

#ifdef __cplusplus
}
#endif

#endif // __FW_H__
