/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include <amxc/amxc_llist.h>
#include <fwrules/fw.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw_rule.h>

#include "fw_rule_priv.h"
#include "test_fw_folder.h"

#ifndef UNUSED
#define UNUSED __attribute__((unused))
#endif

static bool fw_commit_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                               const char* chain, const char* table, int index) {
    char* str_flag = "unknown";

    switch(flag) {
    case FW_RULE_FLAG_NEW:
        str_flag = "new";
        break;
    case FW_RULE_FLAG_MODIFIED:
        str_flag = "modified";
        break;
    case FW_RULE_FLAG_DELETED:
        str_flag = "deleted";
        break;
    case FW_RULE_FLAG_HANDLED:
        str_flag = "handled";
        break;
    case FW_RULE_FLAG_LAST:
        str_flag = "last (nothing)";
        break;
    }

    printf("Event: rule %p, enabled: %d, chain: %s, table: %s, index: %d, flag %s, features: 0x%x\n",
           rule, (int) fw_rule_is_enabled(rule), chain, table, index, str_flag, fw_rule_get_feature_marks((fw_rule_t*) rule));

    return true;
}

void test_fw_folder_new(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;

    retval = fw_folder_new(NULL);
    assert_int_equal(retval, -1);

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    fw_commit(fw_commit_callback);

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    fw_commit(fw_commit_callback);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

void test_fw_folder_get_set_enabled(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    int enabled = 0;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    enabled = fw_folder_get_enabled(NULL);
    assert_int_equal(enabled, 0);

    enabled = fw_folder_get_enabled(folder);
    assert_int_equal(enabled, 0);

    retval = fw_folder_set_enabled(NULL, true);
    assert_int_equal(retval, -1);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);
    enabled = fw_folder_get_enabled(folder);
    assert_int_equal(enabled, 1);

    retval = fw_folder_set_enabled(folder, false);
    assert_int_equal(retval, 0);
    enabled = fw_folder_get_enabled(folder);
    assert_int_equal(enabled, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    fw_commit(fw_commit_callback);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

void test_fw_folder_get_set_feature(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    int feature_requirements = 0;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    retval = fw_folder_set_feature(NULL, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, -1);

    retval = fw_folder_set_feature(NULL, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);

    retval = fw_folder_set_feature(folder, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_LAST));

    feature_requirements = fw_folder_get_feature_requirements(NULL);
    assert_int_equal(feature_requirements, 0);

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_int_equal(feature_requirements, 0);

    retval = fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_true(feature_requirements & (1 << FW_FEATURE_IN_INTERFACE));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_IN_INTERFACE));

    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_true(feature_requirements & (1 << FW_FEATURE_IN_INTERFACE));
    assert_true(feature_requirements & (1 << FW_FEATURE_SOURCE_PORT));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_IN_INTERFACE));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_SOURCE_PORT));
    assert_false(feature_requirements & (1 << FW_FEATURE_PROTOCOL));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_PROTOCOL));

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    fw_commit(fw_commit_callback);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

void test_fw_folder_get_unset_feature(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    int feature_requirements = 0;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_int_equal(feature_requirements, 0);

    retval = fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_ISOLATION);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_WHITELIST);
    assert_int_equal(retval, 0);

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_true(feature_requirements & (1 << FW_FEATURE_IN_INTERFACE));
    assert_true(feature_requirements & (1 << FW_FEATURE_ISOLATION));
    assert_true(feature_requirements & (1 << FW_FEATURE_DSCP));
    assert_true(feature_requirements & (1 << FW_FEATURE_WHITELIST));
    assert_false(feature_requirements & (1 << FW_FEATURE_TARGET));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_IN_INTERFACE));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_ISOLATION));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_DSCP));
    assert_true(fw_feature_contains(feature_requirements, FW_FEATURE_WHITELIST));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_TARGET));

    retval = fw_folder_unset_feature(NULL, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, -1);

    retval = fw_folder_unset_feature(folder, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_true(feature_requirements & (1 << FW_FEATURE_IN_INTERFACE));
    assert_true(feature_requirements & (1 << FW_FEATURE_ISOLATION));
    assert_true(feature_requirements & (1 << FW_FEATURE_DSCP));
    assert_true(feature_requirements & (1 << FW_FEATURE_WHITELIST));
    assert_false(feature_requirements & (1 << FW_FEATURE_TARGET));

    retval = fw_folder_unset_feature(folder, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);
    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_false(feature_requirements & (1 << FW_FEATURE_IN_INTERFACE));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_IN_INTERFACE));

    retval = fw_folder_unset_feature(folder, FW_FEATURE_ISOLATION);
    assert_int_equal(retval, 0);
    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_false(feature_requirements & (1 << FW_FEATURE_ISOLATION));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_ISOLATION));

    retval = fw_folder_unset_feature(folder, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);
    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_false(feature_requirements & (1 << FW_FEATURE_DSCP));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_DSCP));

    retval = fw_folder_unset_feature(folder, FW_FEATURE_WHITELIST);
    assert_int_equal(retval, 0);
    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_false(feature_requirements & (1 << FW_FEATURE_WHITELIST));
    assert_false(fw_feature_contains(feature_requirements, FW_FEATURE_WHITELIST));

    feature_requirements = fw_folder_get_feature_requirements(folder);
    assert_int_equal(feature_requirements, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    fw_commit(fw_commit_callback);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

static bool fw_fetch_default_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                           const char* chain, const char* table, int index) {

    fw_rule_flag_t flags[] = { FW_RULE_FLAG_NEW, FW_RULE_FLAG_DELETED, FW_RULE_FLAG_MODIFIED,
        FW_RULE_FLAG_DELETED, FW_RULE_FLAG_MODIFIED, FW_RULE_FLAG_DELETED };
    static int i = 0;
    int feature_marks = 0;

    //Print event.
    fw_commit_callback(rule, flag, chain, table, index);

    assert_non_null(rule);

    feature_marks = fw_rule_get_feature_marks((fw_rule_t*) rule);
    assert_int_equal(feature_marks, 0);

    assert_int_equal(flag, flags[i++]);
    return true;
}

void test_fw_folder_fetch_default_rule(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    fw_feature_t feature = FW_FEATURE_LAST;
    fw_rule_flag_t flag = FW_RULE_FLAG_NEW;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    rule = fw_folder_fetch_default_rule(NULL);
    assert_null(rule);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    feature = fw_rule_get_current_feature(NULL);
    assert_true(feature == FW_FEATURE_LAST);

    feature = fw_rule_get_current_feature(rule);
    assert_true(feature == FW_FEATURE_LAST);

    flag = fw_rule_get_flag(NULL);
    assert_true(flag == FW_RULE_FLAG_LAST);

    flag = fw_rule_get_flag(rule);
    assert_true(flag == FW_RULE_FLAG_NEW);

    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "INPUT");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Trigger FW_RULE_FLAG_NEW event.
    retval = fw_commit(fw_fetch_default_rule_callback);
    assert_int_equal(retval, 0);
    flag = fw_rule_get_flag(rule);
    assert_true(flag == FW_RULE_FLAG_HANDLED);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Trigger FW_RULE_FLAG_{DELETE,MODIFIED} event.
    retval = fw_commit(fw_fetch_default_rule_callback);
    assert_int_equal(retval, 0);
    flag = fw_rule_get_flag(rule);
    assert_true(flag == FW_RULE_FLAG_HANDLED);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Trigger FW_RULE_FLAG_{DELETE,MODIFIED} event.
    retval = fw_commit(fw_fetch_default_rule_callback);
    assert_int_equal(retval, 0);
    flag = fw_rule_get_flag(rule);
    assert_true(flag == FW_RULE_FLAG_HANDLED);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    //Trigger FW_RULE_FLAG_DELETE event.
    retval = fw_commit(fw_fetch_default_rule_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

void test_fw_folder_push_rule(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    fw_rule_t* faulty_rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    retval = fw_folder_push_rule(NULL, NULL);
    assert_int_equal(retval, -1);

    retval = fw_folder_push_rule(NULL, rule);
    assert_int_equal(retval, -1);

    retval = fw_folder_push_rule(folder, NULL);
    assert_int_equal(retval, -1);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_rule_new(&faulty_rule);
    assert_int_equal(retval, 0);
    assert_non_null(faulty_rule);

    retval = fw_folder_push_rule(folder, faulty_rule);
    assert_int_equal(retval, -1);

    retval = fw_rule_delete(&faulty_rule);
    assert_int_equal(retval, 0);
    assert_null(faulty_rule);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_commit(fw_commit_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    retval = fw_commit(fw_commit_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

static bool fw_fetch_feature_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                           const char* chain, const char* table, int index) {
    static int i = 0;
    fw_rule_flag_t flags[48] = {0};
    int feature_marks[48] = {0};

    flags[0] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[0], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[0], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[0], FW_FEATURE_TARGET);

    flags[1] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[1], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[1], FW_FEATURE_PROTOCOL);

    flags[2] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[2], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[2], FW_FEATURE_TARGET);

    flags[3] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[3], FW_FEATURE_SOURCE_PORT);

    flags[4] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[4], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[4], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[4], FW_FEATURE_TARGET);

    flags[5] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[5], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[5], FW_FEATURE_PROTOCOL);

    flags[6] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[6], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[6], FW_FEATURE_TARGET);

    flags[7] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[7], FW_FEATURE_PROTOCOL);

    flags[8] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[8], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[8], FW_FEATURE_TARGET);

    flags[9] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[9], FW_FEATURE_SOURCE_PORT);

    flags[10] = FW_RULE_FLAG_NEW;
    fw_feature_set(&feature_marks[10], FW_FEATURE_TARGET);

    flags[11] = FW_RULE_FLAG_NEW;
    feature_marks[11] = 0;

    //[ (A1B1C1), (A1B1), (A1C1), (A1), (B1A2C1), (B1A2), (B1C1), (B1), (A2C1), (A2), (C1), (0) ]

    flags[12] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[12], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[12], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[12], FW_FEATURE_TARGET);

    flags[13] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[13], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[13], FW_FEATURE_PROTOCOL);

    flags[14] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[14], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[14], FW_FEATURE_TARGET);

    flags[15] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[15], FW_FEATURE_SOURCE_PORT);

    flags[16] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[16], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[16], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[16], FW_FEATURE_TARGET);

    flags[17] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[17], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[17], FW_FEATURE_PROTOCOL);

    flags[18] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[18], FW_FEATURE_PROTOCOL);
    fw_feature_set(&feature_marks[18], FW_FEATURE_TARGET);

    flags[19] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[19], FW_FEATURE_PROTOCOL);

    flags[20] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[20], FW_FEATURE_SOURCE_PORT);
    fw_feature_set(&feature_marks[20], FW_FEATURE_TARGET);

    flags[21] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[21], FW_FEATURE_SOURCE_PORT);

    flags[22] = FW_RULE_FLAG_DELETED;
    fw_feature_set(&feature_marks[22], FW_FEATURE_TARGET);

    flags[23] = FW_RULE_FLAG_DELETED;
    feature_marks[23] = 0;

    //Print event.
    fw_commit_callback(rule, flag, chain, table, index);

    assert_non_null(rule);
    assert_int_equal(flag, flags[i]);
    assert_int_equal(fw_rule_get_feature_marks((fw_rule_t*) rule), feature_marks[i]);
    i++;
    return true;
}

/**
   For this test, assume:
      FW_FEATURE_SOURCE_PORT as A,
      FW_FEATURE_PROTOCOL as B,
      FW_FEATURE_TARGET as C
      (0) means a rule with no feature set.

      Each feature is followed by an index, A1, A2, B1, B2, ...
      A rule can have a combination of features: (A1B2C1).
 */

void test_fw_folder_fetch_feature_rule(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    int folder_feature_marks = 0;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);

    //Fetch feature rules with faulty arguments.
    rule = fw_folder_fetch_feature_rule(NULL, FW_FEATURE_SOURCE_PORT);
    assert_null(rule);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_LAST);
    assert_null(rule);
    //Non set folder feature.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    assert_null(rule);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "nat");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "OUTPUT");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    /**
       The current folder rules at this point:
        [ (0) ]
     */

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A1
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B1
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A2
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (A1), (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET); //C1
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (A1), (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Three rules with different features are set.
    folder_feature_marks = fw_folder_get_feature_marks(NULL);
    assert_int_equal(folder_feature_marks, 0);

    folder_feature_marks = fw_folder_get_feature_marks(folder);
    assert_true(fw_feature_contains(folder_feature_marks, FW_FEATURE_SOURCE_PORT));
    assert_true(fw_feature_contains(folder_feature_marks, FW_FEATURE_PROTOCOL));
    assert_true(fw_feature_contains(folder_feature_marks, FW_FEATURE_TARGET));
    assert_false(fw_feature_contains(folder_feature_marks, FW_FEATURE_IN_INTERFACE));

    //Fetching the default rule isn't possible anymore, since the folder contains
    //more than one rule.
    rule = fw_folder_fetch_default_rule(folder);
    assert_null(rule);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B2
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (A1B1), (A1), (B1), (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A3
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (A1B1), (A1), (B1A2), (B1), (A2), (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET); //C2
    assert_non_null(rule);

    /**
       The current folder rules at this point:
        [ (A1B1C1), (A1B1), (A1C1), (A1), (B1A2C1), (B1A2), (B1C1), (B1), (A2C1), (A2), (C1), (0) ]
     */

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    retval = fw_commit(fw_fetch_feature_rule_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    retval = fw_commit(fw_fetch_feature_rule_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(amxc_llist_size(fw_rule_get_global_list()), 0);
}

static int test_fw_folder_delete_rules_deleted = 0;

static bool fw_delete_rules_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                     const char* chain, const char* table, int index) {
    if(flag == (FW_RULE_FLAG_DELETED)) {
        test_fw_folder_delete_rules_deleted++;
    }

    //Print event.
    fw_commit_callback(rule, flag, chain, table, index);
    assert_non_null(rule);

    return true;
}

void test_fw_folder_delete_rules(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);

    //The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "INPUT");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A2
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET); //C1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B2
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A3
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET); //C2
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Number of rules: (3A), (2B), (2C) = 3 * 2 * 2 = 12 rules.
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete_rules(NULL);
    assert_int_equal(retval, -1);
    retval = fw_folder_delete_rules(folder);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_feature_marks(folder), 0);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(test_fw_folder_delete_rules_deleted, 12);

    //Must be able to fetch a default rule.
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "raw");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    //Rule is disabled, since folder requirements don't match feature marks.
    assert_false(fw_rule_is_enabled(rule));
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //The next call will delete the default rule and creates a new one.
    //The default rule is deleted, since its flag is still set to FW_RULE_FLAG_NEW.
    //FW_RULE_FLAG_NEW means the rule has not been processed yet. It is safe to
    //delete.
    retval = fw_folder_delete_rules(folder);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);
    //Fetch the same default rule.
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);
}

void test_fw_folder_delete_rules_part2(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_folder_t* folder2 = NULL;
    fw_rule_t* rule = NULL;

    test_fw_folder_delete_rules_deleted = 0;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);
    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);
    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);
    retval = fw_folder_set_enabled(folder2, true);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);

    // add a rule to the first folder
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "INPUT");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);

    // some code might delete all rules of folder2 even if no rules are set
    retval = fw_folder_delete_rules(folder2);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(test_fw_folder_delete_rules_deleted, 0); // the delete callback is not called because only the default rule is deleted

    // it must remain possible to fetch a default rule from folder2
    rule = fw_folder_fetch_default_rule(folder2);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "raw");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);
    retval = fw_folder_delete(&folder2);
    assert_int_equal(retval, 0);
    assert_null(folder2);
    retval = fw_commit(fw_delete_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(test_fw_folder_delete_rules_deleted, 2);
}

static int test_fw_folder_delete_feature_rules_deleted = 0;
static int test_fw_folder_delete_feature_check_source_port = 0;

static bool fw_delete_feature_rules_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                             const char* chain, const char* table, int index) {

    //Print event.
    fw_commit_callback(rule, flag, chain, table, index);
    assert_non_null(rule);

    if(flag == FW_RULE_FLAG_DELETED) {
        if(test_fw_folder_delete_feature_check_source_port) {
            if(fw_feature_contains(fw_rule_get_feature_marks((fw_rule_t*) rule), FW_FEATURE_SOURCE_PORT)) {
                test_fw_folder_delete_feature_rules_deleted++;
            }
        } else {
            test_fw_folder_delete_feature_rules_deleted++;
        }
    }

    return true;
}

void test_fw_folder_delete_feature_rules(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);

    //The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "INPUT");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A2
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET); //C1
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PROTOCOL); //B2
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Fetch a rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT); //A3
    assert_non_null(rule);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Number of rules: (3A), (2B), (1C) = 3 * 2 * 1 = 6 rules.

    //Trigger FW_RULE_FLAG_NEW events.
    retval = fw_commit(fw_delete_feature_rules_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete_feature_rules(NULL, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    retval = fw_folder_delete_feature_rules(folder, FW_FEATURE_LAST);
    assert_int_equal(retval, -1);
    assert_int_equal(test_fw_folder_delete_feature_rules_deleted, 0);
    assert_int_equal(retval, -1);
    retval = fw_folder_delete_feature_rules(folder, FW_FEATURE_DSCP);
    assert_int_equal(retval, -1);
    assert_int_equal(test_fw_folder_delete_feature_rules_deleted, 0);

    retval = fw_folder_delete_feature_rules(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);

    //Trigger FW_RULE_FLAG_DELETED events for FW_FEATURE_SOURCE_PORT (3A).
    //The other rules are disabled, since no FW_FEATURE_SOURCE_PORT rules
    //are present in the folder, while this was set in the requirements.
    test_fw_folder_delete_feature_check_source_port = 1;
    retval = fw_commit(fw_delete_feature_rules_callback);
    assert_int_equal(retval, 0);

    /**
        The number of deleted rules depend on the order of rules added.
        The order:
        A1, B1, A2, C1, B2, A3
        Each time a second feature is added, a new rule is created containing that
        feature, inheriting the existing ones.
        A1, B1
            => default rule
        A2
            => new rule cloned from default rule, featuring A1
        B2
            => new rule cloned from A1, featuring A1B1
            => new rule from default rule, featuring B1
        A3
            => new rule cloned from B1, featuring A2B1
            => new rule from default rule, featuring A2

        The rules in the folder are:
        - A1B1
        - A1
        - A2B1
        - B1
        - A2
        - default rule (without feature marks)

        The rules containing feature A are: A1B1, A1, A2B1 and A2. So 4 rules.
     */
    assert_int_equal(test_fw_folder_delete_feature_rules_deleted, 4);

    retval = fw_folder_delete_rules(NULL);
    assert_int_equal(retval, -1);
    retval = fw_folder_delete_rules(folder);
    assert_int_equal(retval, 0);

    //Trigger FW_RULE_FLAG_DELETED events.
    test_fw_folder_delete_feature_check_source_port = 0;
    retval = fw_commit(fw_delete_feature_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(test_fw_folder_delete_feature_rules_deleted, 6);
    assert_int_equal(fw_folder_get_feature_marks(folder), 0);

    //Must be able to fetch a default rule.
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "raw");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    //Rule is disabled, since folder requirements don't match feature marks.
    assert_false(fw_rule_is_enabled(rule));
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //This deletes the default rule in the background, since it is not processed in the callback function.
    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    assert_null(folder);

    retval = fw_commit(fw_delete_feature_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(test_fw_folder_delete_feature_rules_deleted, 6);
}

static bool fw_folder_global_rules_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                            const char* chain, const char* table, int index) {

    const amxc_llist_t* rules = fw_rule_get_global_list();
    bool found = false;
    int n_found = 0;

    assert_non_null(rules);

    amxc_llist_for_each(it, rules) {
        fw_rule_t* g_rule = fw_rule_container_of(it);

        if(g_rule == rule) {
            found = true;
            n_found++;
        }
    }

    assert_true(found);
    assert_int_equal(n_found, 1);
    fw_commit_callback(rule, flag, chain, table, index);
    return true;
}

void test_fw_folder_global_rules(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_folder_t* folder3 = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);

    fw_folder_set_feature(folder1, FW_FEATURE_DESTINATION);
    fw_folder_set_feature(folder1, FW_FEATURE_POLICY);

    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);

    fw_folder_set_feature(folder2, FW_FEATURE_SOURCE);

    retval = fw_folder_new(&folder3);
    assert_int_equal(retval, 0);

    fw_folder_set_feature(folder3, FW_FEATURE_SOURCE);
    fw_folder_set_feature(folder3, FW_FEATURE_POLICY);

    //Add some rules to folder 1.
    retval = 0;
    rule = fw_folder_fetch_default_rule(folder1);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_in_interface(rule, "ptm0");
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DESTINATION);
    retval |= fw_rule_set_destination(rule, "192.168.1.50");
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_POLICY);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DESTINATION);
    retval |= fw_rule_set_destination(rule, "192.168.1.50");
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_POLICY);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DESTINATION);
    retval |= fw_rule_set_destination(rule, "192.168.1.68");
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_POLICY);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DESTINATION);
    retval |= fw_rule_set_destination(rule, "192.168.1.68");
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_POLICY);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    retval |= fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    //Add some rules to folder 2.
    rule = fw_folder_fetch_default_rule(folder2);
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_in_interface(rule, "eth4");
    retval |= fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    retval = 0;

    //Add some rules to folder 3.
    rule = fw_folder_fetch_default_rule(folder3);
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_rule_set_chain(rule, "PREROUTING");
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);
    retval |= fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder3, FW_FEATURE_SOURCE);
    retval |= fw_rule_set_source(rule, "5.83.47.20");
    retval |= fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    retval = 0;
    rule = fw_folder_fetch_feature_rule(folder3, FW_FEATURE_SOURCE);
    retval |= fw_rule_set_source(rule, "5.83.47.30");
    retval |= fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    retval = fw_commit(fw_folder_global_rules_callback);
    assert_int_equal(retval, 0);

    retval = fw_folder_delete(&folder2);
    retval = fw_folder_delete(&folder1);
    retval = fw_folder_delete(&folder3);

    fw_commit(fw_commit_callback);
}

static int fw_folder_rules_modified_callback_modified = 0;

static bool fw_folder_rules_modified_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                              const char* chain, const char* table, int index) {

    if(FW_RULE_FLAG_MODIFIED == flag) {
        fw_folder_rules_modified_callback_modified++;
    }
    fw_commit_callback(rule, flag, chain, table, index);
    return true;
}

void test_fw_folder_rules_modified(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);

    //The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
    assert_non_null(rule);
    retval = fw_rule_set_source_port(rule, 5000);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    assert_non_null(rule);
    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
    assert_non_null(rule);
    retval = fw_rule_set_source_port(rule, 8000);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    assert_non_null(rule);
    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Apply rules: event contains new rules.
    retval = fw_commit(fw_folder_rules_modified_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_rules_modified_callback_modified, 0);

    //Modify rules
    retval = fw_folder_set_enabled(folder, false);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_rules_modified_callback_modified, 0);

    //Apply rules: modified rules are skipped because the folder is disabled.
    retval = fw_commit(fw_folder_rules_modified_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_rules_modified_callback_modified, 0);

    //Cleanup
    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_delete_feature_rules_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_rules_modified_callback_modified, 0);

}

static bool fw_folder_rules_error_in_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                              const char* chain, const char* table, int index) {

    fw_commit_callback(rule, flag, chain, table, index);

    //Trigger error.
    return false;
}

static void fw_folder_rules_error_in_callback_check_flag(const fw_rule_flag_t flag) {

    amxc_llist_for_each(it, fw_rule_get_global_list()) {
        fw_rule_t* rule = fw_rule_container_of(it);

        assert_non_null(rule);
        fw_commit_callback(rule, fw_rule_get_flag(rule), fw_rule_get_chain(rule), fw_rule_get_table(rule), 1);
        assert_int_equal(flag, fw_rule_get_flag(rule));
    }
}

void test_fw_folder_error_in_callback(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_SOURCE_PORT);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    assert_int_equal(retval, 0);

    //The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "filter");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
    assert_non_null(rule);
    retval = fw_rule_set_source_port(rule, 5000);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    assert_non_null(rule);
    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE_PORT);
    assert_non_null(rule);
    retval = fw_rule_set_source_port(rule, 8000);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    assert_non_null(rule);
    retval = fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Apply rules, but fail in callback. Flag must remain new.
    retval = fw_commit(fw_folder_rules_error_in_callback);
    assert_int_equal(retval, -2);
    fw_folder_rules_error_in_callback_check_flag(FW_RULE_FLAG_NEW);

    //Apply rules without error, the internal flags change only after a successful commit.
    retval = fw_commit(fw_commit_callback);
    assert_int_equal(retval, 0);

    //Modify rules.
    retval = fw_folder_set_enabled(folder, false);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Apply rules, but fail in callback. Flag must remain modified.
    retval = fw_commit(fw_folder_rules_error_in_callback);
    assert_int_equal(retval, -2);
    fw_folder_rules_error_in_callback_check_flag(FW_RULE_FLAG_MODIFIED);

    //Cleanup, but fail in callback. Flags must remain deleted.
    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    retval = fw_commit(fw_folder_rules_error_in_callback);
    assert_int_equal(retval, -2);
    fw_folder_rules_error_in_callback_check_flag(FW_RULE_FLAG_DELETED);

    //Now, cleanup.
    retval = fw_commit(fw_commit_callback);
    assert_int_equal(retval, 0);

}

static int index_table_raw = 0;
static int index_table_mangle = 0;
static int index_table_nat = 0;

static bool test_fw_folder_feature_table_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                  const char* chain, const char* table, int index) {
    int raw = false;
    int mangle = false;
    int nat = false;

    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);
    assert_string_equal(chain, "PREROUTING");

    raw = !strcmp(table, "raw");
    mangle = !strcmp(table, "mangle");
    nat = !strcmp(table, "nat");

    if((flag == FW_RULE_FLAG_NEW) || (flag == FW_RULE_FLAG_MODIFIED)) {

        if(raw) {
            index_table_raw += 1;
            assert_int_equal(index, index_table_raw);
        }

        if(mangle) {
            index_table_mangle += 1;
            assert_int_equal(index, index_table_mangle);
        }

        if(nat) {
            index_table_nat += 1;
            assert_int_equal(index, index_table_nat);
        }
    }

    if(flag == FW_RULE_FLAG_DELETED) {
        assert_int_equal(index, 1);
    }

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

void test_fw_folder_feature_table(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);
    assert_non_null(folder);

    //Set folder requirements.
    retval = fw_folder_set_feature(folder, FW_FEATURE_TABLE);
    assert_int_equal(retval, 0);

    //The defaults, creating rules for two different tables. Do not set table.
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "raw");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "nat");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "raw");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "nat");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    //Feature rule.
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TABLE);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    index_table_raw = 0;
    index_table_mangle = 0;
    index_table_nat = 0;

    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //New events
    retval = fw_commit(test_fw_folder_feature_table_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 2);
    assert_int_equal(index_table_mangle, 3);
    assert_int_equal(index_table_nat, 2);

    index_table_raw = 0;
    index_table_mangle = 0;
    index_table_nat = 0;

    //Modified rules are only seen in the callback when they are enabled.
    //Therefore, toggle the enable flag twice.

    retval = fw_folder_set_enabled(folder, false);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_enabled(folder, true);
    assert_int_equal(retval, 0);

    //Modified events
    retval = fw_commit(test_fw_folder_feature_table_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 2);
    assert_int_equal(index_table_mangle, 3);
    assert_int_equal(index_table_nat, 2);

    index_table_raw = 0;
    index_table_mangle = 0;
    index_table_nat = 0;

    //Cleanup
    retval = fw_folder_delete(&folder);
    assert_int_equal(retval, 0);
    retval = fw_commit(test_fw_folder_feature_table_callback);
    assert_int_equal(retval, 0);

}

static bool test_fw_folder_commit_index_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                 const char* chain, const char* table, int index) {
    int raw = false;
    int mangle = false;
    int nat = false;

    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);
    assert_string_equal(chain, "PREROUTING");

    raw = !strcmp(table, "raw");
    mangle = !strcmp(table, "mangle");
    nat = !strcmp(table, "nat");

    if((flag == FW_RULE_FLAG_NEW) || (flag == FW_RULE_FLAG_MODIFIED)) {

        if(raw) {
            index_table_raw += 1;
            assert_int_equal(index, index_table_raw);
        }

        if(mangle) {
            index_table_mangle += 1;
            assert_int_equal(index, index_table_mangle);
        }

        if(nat) {
            index_table_nat += 1;
            assert_int_equal(index, index_table_nat);
        }
    }

    if(flag == FW_RULE_FLAG_DELETED) {
        if(raw) {
            index_table_raw -= 1;
        }

        if(mangle) {
            index_table_mangle -= 1;
        }

        if(nat) {
            index_table_nat -= 1;
        }
    }

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

static bool test_fw_folder_commit_index_callback_expect_index_1(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                const char* chain, const char* table, int index) {
    bool retval = false;

    retval = test_fw_folder_commit_index_callback(rule, flag, chain, table, index);
    assert_true(retval);
    assert_int_equal(index, 1);

    return true;
}

static bool test_fw_folder_commit_index_callback_expect_index_2(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                const char* chain, const char* table, int index) {
    bool retval = false;

    retval = test_fw_folder_commit_index_callback(rule, flag, chain, table, index);
    assert_true(retval);
    assert_int_equal(index, 2);

    return true;
}

static bool test_fw_folder_commit_index_callback_expect_index_3(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                const char* chain, const char* table, int index) {
    bool retval = false;

    retval = test_fw_folder_commit_index_callback(rule, flag, chain, table, index);
    assert_true(retval);
    assert_int_equal(index, 3);

    return true;
}

void test_fw_folder_commit_index(UNUSED void** state) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    fw_folder_t* folders[3] = {0};
    const char* tables[] = { "raw", "mangle", "nat" };
    int num_of_folders = sizeof(folders) / sizeof(folders[0]);
    int num_of_tables = sizeof(tables) / sizeof(tables[0]);
    int i = 0;
    int j = 0;

    /**
     * Create three folders, each containing three rules.
     * Folder:
     *  rule: (raw, PREROUTING)
     *  rule: (mangle, PREROUTING)
     *  rule: (nat, PREROUTING)
     *
     *  Indices for each rule in a folder, in order of creation:
     *   folders[0]: index 1
     *   folders[1]: index 2
     *   folders[2]: index 3
     */
    for(i = 0; i < num_of_folders; i++) {
        retval = fw_folder_new(&folders[i]);
        assert_int_equal(retval, 0);
        assert_non_null(folders[i]);

        retval = fw_folder_set_feature(folders[i], FW_FEATURE_TABLE);
        assert_int_equal(retval, 0);

        rule = fw_folder_fetch_default_rule(folders[i]);
        assert_non_null(rule);
        retval = fw_rule_set_chain(rule, "PREROUTING");
        assert_int_equal(retval, 0);
        retval = fw_folder_push_rule(folders[i], rule);
        assert_int_equal(retval, 0);

        //Feature rule.
        for(j = 0; j < num_of_tables; j++) {
            rule = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_TABLE);
            assert_non_null(rule);
            retval = fw_rule_set_table(rule, tables[j]);
            assert_int_equal(retval, 0);
            retval = fw_folder_push_rule(folders[i], rule);
            assert_int_equal(retval, 0);
        }

        retval = fw_folder_set_enabled(folders[i], true);
        assert_int_equal(retval, 0);
    }

    index_table_raw = 0;
    index_table_mangle = 0;
    index_table_nat = 0;

    //New events: all rules are created.
    retval = fw_commit(test_fw_folder_commit_index_callback);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 3);
    assert_int_equal(index_table_mangle, 3);
    assert_int_equal(index_table_nat, 3);

    //Destroy folders[0]. All rules with index 1 are removed.
    retval = fw_folder_delete(&folders[0]);                                  //Rules marked for deletion.
    assert_int_equal(retval, 0);
    retval = fw_commit(test_fw_folder_commit_index_callback_expect_index_1); //Peform deletion.
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 2);
    assert_int_equal(index_table_mangle, 2);
    assert_int_equal(index_table_nat, 2);

    /**
     * At this point, only folders[1] and folders[2] exist.
     *  Indices for each rule in a folder:
     *   folders[0]: does not exist.
     *   folders[1]: index 1
     *   folders[2]: index 2
     *
     * Note that the indices are renumbered.
     *
     * Recreate folders[0]. Rules for folders[0] are appended and get index 3.
     *  Indices for each rule in a folder after recreating folders[0]:
     *   folders[1]: index 1
     *   folders[2]: index 2
     *   folders[0]: index 3
     */
    retval = fw_folder_new(&folders[0]);
    assert_int_equal(retval, 0);
    assert_non_null(folders[0]);
    retval = fw_folder_set_feature(folders[0], FW_FEATURE_TABLE);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_default_rule(folders[0]);
    assert_non_null(rule);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folders[0], rule);
    assert_int_equal(retval, 0);
    for(j = 0; j < num_of_tables; j++) {
        rule = fw_folder_fetch_feature_rule(folders[0], FW_FEATURE_TABLE);
        assert_non_null(rule);
        retval = fw_rule_set_table(rule, tables[j]);
        assert_int_equal(retval, 0);
        retval = fw_folder_push_rule(folders[0], rule);
        assert_int_equal(retval, 0);
    }
    retval = fw_folder_set_enabled(folders[0], true);
    assert_int_equal(retval, 0);

    //New events: only rules for folders[0] are created with index 3.
    retval = fw_commit(test_fw_folder_commit_index_callback_expect_index_3);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 3);
    assert_int_equal(index_table_mangle, 3);
    assert_int_equal(index_table_nat, 3);

    //Destroy folders[2]. All rules with index 2 are removed.
    retval = fw_folder_delete(&folders[2]);
    assert_int_equal(retval, 0);
    retval = fw_commit(test_fw_folder_commit_index_callback_expect_index_2);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 2);
    assert_int_equal(index_table_mangle, 2);
    assert_int_equal(index_table_nat, 2);

    /**
     * At this point, only folders[0] and folders[2] exist.
     *  Indices for each rule in a folder:
     *   folders[1]: index 1
     *   folders[2]: does not exist
     *   folders[0]: index 2
     */

    //Destroy folders[1]. All rules with index 1 are removed.
    retval = fw_folder_delete(&folders[1]);
    assert_int_equal(retval, 0);
    retval = fw_commit(test_fw_folder_commit_index_callback_expect_index_1);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 1);
    assert_int_equal(index_table_mangle, 1);
    assert_int_equal(index_table_nat, 1);

    /**
     * At this point, only folders[0] and folders[2] exist.
     *  Indices for each rule in a folder:
     *   folders[0]: index 1
     *   folders[1]: does not exist
     *   folders[2]: does not exist
     */

    //Destroy folders[0]. All rules with index 1 are removed.
    retval = fw_folder_delete(&folders[0]);
    assert_int_equal(retval, 0);
    retval = fw_commit(test_fw_folder_commit_index_callback_expect_index_1);
    assert_int_equal(retval, 0);
    assert_int_equal(index_table_raw, 0);
    assert_int_equal(index_table_mangle, 0);
    assert_int_equal(index_table_nat, 0);
}

static int test_fw_folder_ordered_state = 0;

static bool test_fw_folder_ordered_callback(const fw_rule_t* const rule,
                                            const fw_rule_flag_t flag, const char* chain, const char* table, int index) {

    fw_commit_callback(rule, flag, chain, table, index);

    if(test_fw_folder_ordered_state == 0) {
        //All rules are new.
        assert_int_equal(flag, FW_RULE_FLAG_NEW);

        if(index == 1) {
            assert_int_equal(fw_rule_get_target_dscp_option(rule), 48);
        } else if((index == 2) || (index == 3)) {
            assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
        } else if(index == 4) {
            assert_int_equal(fw_rule_get_target_dscp_option(rule), 58);
        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 1) {
        //All rules in folder 1 are removed.
        if(flag == FW_RULE_FLAG_DELETED) {

            if(index == 1) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 48);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 2) {
        //Rules in folder 1 are added again.
        if(flag == FW_RULE_FLAG_NEW) {
            assert_int_equal(index, 1);
            assert_int_equal(fw_rule_get_target_dscp_option(rule), 48);

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 3) {
        //Folder 3 is moved before folder 2.
        if(flag == FW_RULE_FLAG_DELETED) {

            if(index == 4) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 58);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else if(flag == FW_RULE_FLAG_NEW) {

            if(index == 2) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 58);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 4) {
        if(flag == FW_RULE_FLAG_DELETED) {

            if((index == 3) || (index == 4)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else if(flag == FW_RULE_FLAG_NEW) {

            if((index == 1) || (index == 2)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 5) {
        if(flag == FW_RULE_FLAG_DELETED) {

            if((index == 1) || (index == 2)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else if(flag == FW_RULE_FLAG_NEW) {

            if((index == 3) || (index == 4)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 6) {
        if(flag == FW_RULE_FLAG_DELETED) {

            if((index == 3) || (index == 4)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else if(flag == FW_RULE_FLAG_NEW) {

            if((index == 1) || (index == 2)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 7) {
        if(flag == FW_RULE_FLAG_DELETED) {

            if((index == 1) || (index == 2)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else if(flag == FW_RULE_FLAG_NEW) {

            if((index == 2) || (index == 3)) {
                assert_int_equal(fw_rule_get_target_dscp_option(rule), 52);
            } else {
                //Should not get here.
                assert_true(false);
            }

        } else {
            //Should not get here.
            assert_true(false);
        }

    } else if(test_fw_folder_ordered_state == 8) {
        //Should not get here since nothing is changed.
        assert_true(false);
    }

    return true;
}

void test_fw_folder_ordered(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_folder_t* folder3 = NULL;
    fw_rule_t* rule = NULL;
    uint32_t order = 0;

    //Some spot checks.
    retval = fw_folder_set_order(NULL, 0);
    assert_int_equal(retval, -1);
    retval = fw_folder_set_order(NULL, 1);
    assert_int_equal(retval, -1);
    order = fw_folder_get_order(NULL);
    assert_int_equal(order, 0);

    //Init folder 1
    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);
    assert_non_null(folder1);

    rule = fw_folder_fetch_default_rule(folder1);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_dscp(rule, 48);
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder1, true);
    assert_int_equal(retval, 0);

    order = fw_folder_get_order(folder1);
    assert_int_equal(order, 1);

    //Init folder 2
    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    retval = fw_folder_set_feature(folder2, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_default_rule(folder2);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_dscp(rule, 52);
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_feature_rule(folder2, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule);
    retval = fw_rule_set_in_interface(rule, "eth0");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_feature_rule(folder2, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule);
    retval = fw_rule_set_in_interface(rule, "eth1");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder2, true);
    assert_int_equal(retval, 0);

    order = fw_folder_get_order(folder2);
    assert_int_equal(order, 2);

    //Init folder 3
    retval = fw_folder_new(&folder3);
    assert_int_equal(retval, 0);
    assert_non_null(folder3);

    rule = fw_folder_fetch_default_rule(folder3);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_dscp(rule, 58);
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder3, true);
    assert_int_equal(retval, 0);

    order = fw_folder_get_order(folder3);
    assert_int_equal(order, 3);

    test_fw_folder_ordered_state = 0;
    fw_commit(test_fw_folder_ordered_callback);

    //Disable folder 1 and delete rules.
    retval = fw_folder_set_enabled(folder1, false);
    assert_int_equal(retval, 0);
    retval = fw_folder_delete_rules(folder1);
    assert_int_equal(retval, 0);

    test_fw_folder_ordered_state = 1;
    fw_commit(test_fw_folder_ordered_callback);

    //Add rules again to folder 1.
    rule = fw_folder_fetch_default_rule(folder1);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_dscp(rule, 48);
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_enabled(folder1, true);
    assert_int_equal(retval, 0);

    test_fw_folder_ordered_state = 2;
    fw_commit(test_fw_folder_ordered_callback);

    //Move folder 3 before folder 2.
    retval = fw_folder_set_order(folder3, 2);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder1), 1);
    assert_int_equal(fw_folder_get_order(folder3), 2);
    assert_int_equal(fw_folder_get_order(folder2), 3);

    test_fw_folder_ordered_state = 3;
    fw_commit(test_fw_folder_ordered_callback);

    //Move folder 2 before folder 1.
    retval = fw_folder_set_order(folder2, 1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder2), 1);
    assert_int_equal(fw_folder_get_order(folder1), 2);
    assert_int_equal(fw_folder_get_order(folder3), 3);

    test_fw_folder_ordered_state = 4;
    fw_commit(test_fw_folder_ordered_callback);

    //Move folder 2 after folder 3.
    retval = fw_folder_set_order(folder2, 3);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder1), 1);
    assert_int_equal(fw_folder_get_order(folder3), 2);
    assert_int_equal(fw_folder_get_order(folder2), 3);

    test_fw_folder_ordered_state = 5;
    fw_commit(test_fw_folder_ordered_callback);

    //Move folder 2 before folder 1.
    retval = fw_folder_set_order(folder2, 1);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder2), 1);
    assert_int_equal(fw_folder_get_order(folder1), 2);
    assert_int_equal(fw_folder_get_order(folder3), 3);

    test_fw_folder_ordered_state = 6;
    fw_commit(test_fw_folder_ordered_callback);

    //Move folder 2 after folder 1.
    retval = fw_folder_set_order(folder2, 2);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder1), 1);
    assert_int_equal(fw_folder_get_order(folder2), 2);
    assert_int_equal(fw_folder_get_order(folder3), 3);

    test_fw_folder_ordered_state = 7;
    fw_commit(test_fw_folder_ordered_callback);

    //Set folder's 3 order too large.
    retval = fw_folder_set_order(folder3, 200);
    assert_int_equal(retval, 0);
    assert_int_equal(fw_folder_get_order(folder1), 1);
    assert_int_equal(fw_folder_get_order(folder2), 2);
    assert_int_equal(fw_folder_get_order(folder3), 3);

    test_fw_folder_ordered_state = 8;
    fw_commit(test_fw_folder_ordered_callback);

    //Must be able to fetch a default rule.
    retval = fw_folder_set_enabled(folder1, false);
    assert_int_equal(retval, 0);
    retval = fw_folder_delete_rules(folder1);
    assert_int_equal(retval, 0);
    fw_commit(fw_commit_callback);
    retval = fw_folder_set_order(folder1, 2);
    assert_int_equal(retval, 0);
    rule = fw_folder_fetch_default_rule(folder1);
    assert_non_null(rule);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_target_dscp(rule, 48);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_enabled(folder1, true);
    assert_int_equal(retval, 0);
    fw_commit(fw_commit_callback);

    //Deinit
    retval = fw_folder_delete(&folder1);
    assert_int_equal(retval, 0);
    assert_null(folder1);

    retval = fw_folder_delete(&folder2);
    assert_int_equal(retval, 0);
    assert_null(folder2);

    retval = fw_folder_delete(&folder3);
    assert_int_equal(retval, 0);
    assert_null(folder3);

    fw_commit(fw_commit_callback);
}

static bool fw_commit_similar_chains_tables_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                     const char* chain, const char* table, int index) {

    static int nof_rules_added = 0;
    static int nof_rules_deleted = 0;

    if(FW_RULE_FLAG_NEW == flag) {

        switch(nof_rules_added) {
        case 0:
        case 1:
        case 2:
        case 3:
            assert_string_equal(table, "mangle_dscp");
            assert_string_equal(chain, "FORWARD_dscp");
            break;
        case 4:
        case 5:
            assert_string_equal(table, "mangle");
            assert_string_equal(chain, "FORWARD");
            break;
        default:
            assert("It looks like more than 6 rules were added. We should not get here");
        }

        nof_rules_added++;

    } else if(FW_RULE_FLAG_DELETED == flag) {

        switch(nof_rules_deleted) {
        case 0:
        case 1:
        case 2:
        case 3:
            assert_string_equal(table, "mangle_dscp");
            assert_string_equal(chain, "FORWARD_dscp");
            break;
        case 4:
        case 5:
            assert_string_equal(table, "mangle");
            assert_string_equal(chain, "FORWARD");
            break;
        default:
            assert("It looks like more than 6 rules were deleted. We should not get here");
        }

        nof_rules_deleted++;
    }

    return fw_commit_callback(rule, flag, chain, table, index);
}

void test_fw_folder_similar_tables_chains(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_folder_t* folder3 = NULL;
    fw_rule_t* rule = NULL;

    //Init folder 1
    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);
    assert_non_null(folder1);

    retval = fw_folder_set_feature(folder1, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_default_rule(folder1);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle_dscp");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD_dscp");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    //Rule 1
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 48);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    //Rule 2
    rule = fw_folder_fetch_feature_rule(folder1, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 49);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder1, rule);
    assert_int_equal(retval, 0);

    //Init folder 2
    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    retval = fw_folder_set_feature(folder2, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_default_rule(folder2);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    //Rule 3
    rule = fw_folder_fetch_feature_rule(folder2, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 50);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    //Rule 4
    rule = fw_folder_fetch_feature_rule(folder2, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 51);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule);
    assert_int_equal(retval, 0);

    //Init folder 3, same table and chain as folder1.
    retval = fw_folder_new(&folder3);
    assert_int_equal(retval, 0);
    assert_non_null(folder3);

    retval = fw_folder_set_feature(folder3, FW_FEATURE_DSCP);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_default_rule(folder3);
    assert_non_null(rule);

    retval = fw_rule_set_table(rule, "mangle_dscp");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_chain(rule, "FORWARD_dscp");
    assert_int_equal(retval, 0);

    retval = fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    //Rule 5
    rule = fw_folder_fetch_feature_rule(folder3, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 52);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    //Rule 6
    rule = fw_folder_fetch_feature_rule(folder3, FW_FEATURE_DSCP);
    assert_non_null(rule);
    retval = fw_rule_set_dscp(rule, 53);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder3, rule);
    assert_int_equal(retval, 0);

    //Enable folders
    retval = fw_folder_set_enabled(folder1, true);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_enabled(folder2, true);
    assert_int_equal(retval, 0);
    retval = fw_folder_set_enabled(folder3, true);
    assert_int_equal(retval, 0);

    fw_commit(fw_commit_similar_chains_tables_callback);

    //Deinit
    retval = fw_folder_delete(&folder1);
    assert_int_equal(retval, 0);
    assert_null(folder1);

    retval = fw_folder_delete(&folder2);
    assert_int_equal(retval, 0);
    assert_null(folder2);

    retval = fw_folder_delete(&folder3);
    assert_int_equal(retval, 0);
    assert_null(folder3);

    fw_commit(fw_commit_similar_chains_tables_callback);
}

static bool test_fw_folder_can_delete_rule_after_disabled_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                        const char* chain, const char* table, int index) {
    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);
    assert_string_equal(chain, "PREROUTING");

    check_expected(index);

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

/*
   Summary:
   - Create a folder with unfulfilled feature requirements and enable it.
   - Create another folder with fulfilled feature requirements and enable it.
   - Remove the folder with unfulfilled feature requirements and check that the
    rule from the folder is correctly removed (with the correct index)
 */
void test_fw_folder_can_delete_folder_after_disabled_folder(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_rule_t* rule1 = NULL;
    fw_rule_t* rule2 = NULL;

    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);
    assert_non_null(folder1);

    // Set folder requirements
    retval = fw_folder_set_feature(folder1, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    // The defaults
    rule1 = fw_folder_fetch_default_rule(folder1);
    assert_non_null(rule1);
    retval = fw_rule_set_chain(rule1, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule1, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder1, rule1);
    assert_int_equal(retval, 0);

    // Enable folder without setting feature
    fw_folder_set_enabled(folder1, true);
    fw_commit(test_fw_folder_can_delete_rule_after_disabled_rule_callback);
    assert_false(fw_rule_is_enabled(rule1));

    // Configure second folder with the feature set
    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    // Set folder requirements
    retval = fw_folder_set_feature(folder2, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    // The defaults
    rule2 = fw_folder_fetch_default_rule(folder2);
    assert_non_null(rule2);
    retval = fw_rule_set_chain(rule2, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule2, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule2);
    assert_int_equal(retval, 0);

    // Feature rule
    rule2 = fw_folder_fetch_feature_rule(folder2, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule2);
    retval = fw_rule_set_in_interface(rule2, "eth0");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder2, rule2);
    assert_int_equal(retval, 0);

    // Enable folder after feature is set
    fw_folder_set_enabled(folder2, true);
    expect_value(test_fw_folder_can_delete_rule_after_disabled_rule_callback, index, 1);
    fw_commit(test_fw_folder_can_delete_rule_after_disabled_rule_callback);
    assert_true(fw_rule_is_enabled(rule2));

    // Now delete folder2 which should delete the rule at index 1
    // Before the fix, it would try to delete the rule at index 2, because
    // there was a disabled rule at index 1
    fw_folder_delete(&folder2);
    expect_value(test_fw_folder_can_delete_rule_after_disabled_rule_callback, index, 1);
    fw_commit(test_fw_folder_can_delete_rule_after_disabled_rule_callback);

    fw_folder_delete(&folder1);
    fw_commit(test_fw_folder_can_delete_rule_after_disabled_rule_callback);
}

void test_fw_folder_can_set_order_after_deleting_folder(UNUSED void** state) {
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_folder_t* folder3 = NULL;

    fw_folder_new(&folder1);
    fw_folder_new(&folder2);
    fw_folder_new(&folder3);

    fw_folder_delete(&folder1);

    // Order wasn't updated yet
    assert_int_equal(fw_folder_get_order(folder2), 2);
    assert_int_equal(fw_folder_get_order(folder3), 3);

    // Try to update order
    assert_int_equal(fw_folder_set_order(folder2, 1), 0);
    assert_int_equal(fw_folder_get_order(folder2), 1);
    assert_int_equal(fw_folder_get_order(folder3), 2);

    fw_folder_delete(&folder2);
    fw_folder_delete(&folder3);
}

static int test_string_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    const char* actual_value = (const char*) value;
    char* expected_value = (char*) check_value_data;

    assert_non_null(actual_value);
    assert_non_null(expected_value);

    printf("%s: Expected value = %s, actual value = %s\n", __func__, expected_value, actual_value);
    assert_string_equal(actual_value, expected_value);
    free(expected_value);
    return 1;
}

static void test_fw_check_in_interface(const char* intf) {
    check_expected(intf);
}

static bool test_fw_folder_deleting_rules_respects_order_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                  const char* chain, const char* table, int index) {
    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);
    assert_string_equal(chain, "PREROUTING");

    check_expected(index);
    test_fw_check_in_interface(fw_rule_get_in_interface(rule));

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

static void test_fw_folder_deleting_rules_respects_order_add_rule(fw_folder_t* folder, const char* interface) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    // Set folder requirements
    retval = fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    // The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    // Feature rule
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule);
    retval = fw_rule_set_in_interface(rule, interface);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
}

void test_fw_folder_deleting_rules_respects_order(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    const char* intf_folder1 = "eth1";
    const char* intf_folder2 = "eth2";

    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);
    assert_non_null(folder1);

    // Configure second folder
    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    test_fw_folder_deleting_rules_respects_order_add_rule(folder2, intf_folder2);

    // Set order and enable folders
    fw_folder_set_order(folder1, 1);
    fw_folder_set_order(folder2, 2);
    fw_folder_set_enabled(folder1, true);
    fw_folder_set_enabled(folder2, true);

    // Commit rules, which will only handle rule for folder2, since folder1 has flag NEW
    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder2));
    fw_commit(test_fw_folder_deleting_rules_respects_order_callback);

    // Now delete ALL rules
    fw_folder_delete_rules(folder1);
    fw_folder_delete_rules(folder2);
    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 1); // DEL rule from folder2
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder2));
    fw_commit(test_fw_folder_deleting_rules_respects_order_callback);

    // Now we will start using folder 1
    test_fw_folder_deleting_rules_respects_order_add_rule(folder1, intf_folder1);
    test_fw_folder_deleting_rules_respects_order_add_rule(folder2, intf_folder2);

    // Set order and enable folders
    fw_folder_set_order(folder1, 1);
    fw_folder_set_order(folder2, 2);
    fw_folder_set_enabled(folder1, true);
    fw_folder_set_enabled(folder2, true);

    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 1); // ADD rule from folder1
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder1));
    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 2); // ADD rule from folder2
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder2));
    fw_commit(test_fw_folder_deleting_rules_respects_order_callback);

    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder1));
    expect_value(test_fw_folder_deleting_rules_respects_order_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf_folder2));
    fw_folder_delete(&folder1);
    fw_folder_delete(&folder2);
    fw_commit(test_fw_folder_deleting_rules_respects_order_callback);
}

static bool test_fw_folder_reorder_folder_multiple_rules_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                                                  const char* chain, const char* table, int index) {
    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);

    check_expected(index);
    test_fw_check_in_interface(fw_rule_get_in_interface(rule));

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

static void test_fw_folder_reorder_folder_multiple_rules_add_rules(fw_folder_t* folder, const char* intf1, const char* intf2) {
    int retval = -1;
    fw_rule_t* rule = NULL;

    // Set folder requirements
    retval = fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);
    assert_int_equal(retval, 0);

    // The defaults
    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);
    retval = fw_rule_set_chain(rule, "PREROUTING");
    assert_int_equal(retval, 0);
    retval = fw_rule_set_table(rule, "mangle");
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    // Feature rule 1
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule);
    retval = fw_rule_set_in_interface(rule, intf1);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    // Feature rule 2
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    assert_non_null(rule);
    retval = fw_rule_set_in_interface(rule, intf2);
    assert_int_equal(retval, 0);
    retval = fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);
}

void test_fw_folder_reorder_folder_multiple_rules(UNUSED void** state) {
    int retval = -1;
    fw_folder_t* folder1 = NULL;
    fw_folder_t* folder2 = NULL;
    fw_folder_t* folder3 = NULL;
    const char* intf1 = "eth1";
    const char* intf2 = "eth2";
    const char* intf3 = "eth3";
    const char* intf4 = "eth4";

    // Create 3 folders
    retval = fw_folder_new(&folder1);
    assert_int_equal(retval, 0);
    assert_non_null(folder1);

    retval = fw_folder_new(&folder2);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    retval = fw_folder_new(&folder3);
    assert_int_equal(retval, 0);
    assert_non_null(folder2);

    // Add 2 rules to folder 1 and folder 2
    test_fw_folder_reorder_folder_multiple_rules_add_rules(folder1, intf1, intf2);
    test_fw_folder_reorder_folder_multiple_rules_add_rules(folder2, intf3, intf4);
    fw_folder_set_enabled(folder1, true);
    fw_folder_set_enabled(folder2, true);

    // Now reorder the folders
    fw_folder_set_order(folder2, UINT16_MAX);
    fw_folder_set_order(folder1, fw_folder_get_order(folder2) - 1);

    // Commit, we still expect the rules from folder1 to come before those of folder2
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf1));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 2);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf2));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 3);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf3));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 4);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf4));
    fw_commit(test_fw_folder_reorder_folder_multiple_rules_callback);

    // Clean everything up
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf1));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf2));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf3));
    expect_value(test_fw_folder_reorder_folder_multiple_rules_callback, index, 1);
    expect_check(test_fw_check_in_interface, intf, test_string_equal_check, strdup(intf4));
    fw_folder_delete(&folder1);
    fw_folder_delete(&folder2);
    fw_folder_delete(&folder3);
    fw_commit(test_fw_folder_reorder_folder_multiple_rules_callback);
}

static void test_fw_check_physdev_in(const char* intf) {
    check_expected(intf);
}

static bool test_fw_physdev_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                                          const char* chain, const char* table, int index) {
    assert_non_null(rule);
    assert_non_null(chain);
    assert_non_null(table);

    check_expected(index);
    test_fw_check_physdev_in(fw_rule_get_physdev_in(rule));

    fw_commit_callback(rule, flag, chain, table, index);

    return true;
}

void test_fw_folder_feature_physdev(UNUSED void** state) {
    int retval = 0;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    uint32_t dscp = 40;
    const char* intf1 = "eth1";
    const char* intf2 = "eth2";

    retval = fw_folder_new(&folder);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_default_rule(folder);
    assert_non_null(rule);

    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_table(rule, "mangle");
    retval |= fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval |= fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    retval |= fw_rule_set_target_dscp(rule, dscp);
    retval |= fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    retval = fw_folder_set_feature(folder, FW_FEATURE_PHYSDEV_IN);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_IN);
    retval |= fw_rule_set_physdev_in(rule, intf1);
    retval |= fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_IN);
    retval |= fw_rule_set_physdev_in(rule, intf2);
    retval |= fw_folder_push_rule(folder, rule);
    assert_int_equal(retval, 0);

    // Check the expected indexes and interfaces
    expect_value(test_fw_physdev_rule_callback, index, 1);
    expect_check(test_fw_check_physdev_in, intf, test_string_equal_check, strdup(intf1));
    expect_value(test_fw_physdev_rule_callback, index, 2);
    expect_check(test_fw_check_physdev_in, intf, test_string_equal_check, strdup(intf2));

    retval |= fw_folder_set_enabled(folder, true);
    retval |= fw_commit(test_fw_physdev_rule_callback);
    assert_int_equal(retval, 0);

    fw_folder_delete(&folder);

    expect_value(test_fw_physdev_rule_callback, index, 1);
    expect_check(test_fw_check_physdev_in, intf, test_string_equal_check, strdup(intf1));
    expect_value(test_fw_physdev_rule_callback, index, 1);
    expect_check(test_fw_check_physdev_in, intf, test_string_equal_check, strdup(intf2));
    retval |= fw_commit(test_fw_physdev_rule_callback);
    assert_int_equal(retval, 0);
}
