/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__FW_RULE_PRIV_H__)
#define __FW_RULE_PRIV_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <fwrules/fw_features.h>
#include <fwrules/fw_rule.h>

typedef struct _std_icmp_pair {
    int type;
    int code;
} std_icmp_pair_t;


int fw_rule_init(fw_rule_t* rule);
int fw_rule_deinit(fw_rule_t* rule);
int fw_rule_clone(fw_rule_t** dest, fw_rule_t* const src);

int fw_rule_set_feature_mark(fw_rule_t* const rule, fw_feature_t feature);
int fw_rule_set_flag(fw_rule_t* const rule, const fw_rule_flag_t flag);
int fw_rule_unset_flag(fw_rule_t* const rule, const fw_rule_flag_t flag);
int fw_rule_get_feature_marks(fw_rule_t* const rule);
fw_feature_t fw_rule_get_current_feature(const fw_rule_t* const rule);
int fw_rule_set_current_feature(fw_rule_t* const rule, const fw_feature_t feature);
int fw_rule_sync(fw_rule_t* const dest, const fw_rule_t* const src);

void fw_rule_set_traversed(fw_rule_t* const rule, const int traversed);
int fw_rule_get_traversed(const fw_rule_t* const rule);

amxc_llist_it_t* fw_rule_get_list_iterator(fw_rule_t* const rule);
amxc_llist_it_t* fw_rule_get_global_list_iterator(fw_rule_t* const rule);
fw_rule_t* fw_rule_container_of_it(amxc_llist_it_t* it);
fw_rule_t* fw_rule_container_of(amxc_llist_it_t* it);
const amxc_llist_t* fw_rule_get_global_list(void);

//Helper functions.
int fw_rule_set_old_table(fw_rule_t* const rule, const char* old_table);
int fw_rule_set_old_chain(fw_rule_t* const rule, const char* old_chain);
const char* fw_rule_get_old_table(const fw_rule_t* const rule);
const char* fw_rule_get_old_chain(const fw_rule_t* const rule);


#ifdef __cplusplus
}
#endif

#endif // __FW_RULE_PRIV_H
