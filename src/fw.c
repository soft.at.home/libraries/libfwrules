/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc_llist.h>
#include <fwrules/fw.h>
#include <fwrules/fw_rule.h>

#include "fw_rule_priv.h"

#define FW_TRAVERSED_DEL 0x1
#define FW_TRAVERSED_ADD 0x2

#define FW_COMMIT_OK       0
#define FW_COMMIT_ERROR    -1
#define FW_COMMIT_CB_ERROR -2

int fw_commit(fw_callback_t cb) {
    int retval = FW_COMMIT_ERROR;
    const amxc_llist_t* rules = NULL;
    fw_rule_t* rule = NULL;
    fw_rule_flag_t flag = FW_RULE_FLAG_LAST;
    amxc_llist_it_t* next_chain_it = NULL;
    amxc_llist_it_t* inner_it = NULL;
    char current_table[FW_RULE_TABLE_LEN];
    char current_chain[FW_RULE_CHAIN_LEN];
    int index = 0;
    int traversed = 0;
    bool callback_failed = false;
    bool get_old_table_chain = false;

    when_null(cb, exit);

    rules = fw_rule_get_global_list();
    when_null(rules, exit);

    /*
     * First loop: delete rules which are modified or deleted.
     * Second loop: add rules which are new or modified.
     */
    for(traversed = FW_TRAVERSED_DEL; traversed <= FW_TRAVERSED_ADD; traversed++) {
        index = 0;
        inner_it = amxc_llist_get_first(rules);
        get_old_table_chain = (traversed == FW_TRAVERSED_DEL);

        current_table[0] = '\0';
        current_chain[0] = '\0';

        while(inner_it || next_chain_it) {
            const char* table = NULL;
            const char* chain = NULL;

            //New table - chain combo.
            if((NULL == inner_it) && (NULL != next_chain_it)) {
                inner_it = next_chain_it;
                next_chain_it = NULL;

                index = 0;

                current_table[0] = '\0';
                current_chain[0] = '\0';
            }

            rule = fw_rule_container_of(inner_it);

            //Prepare the next rule to process.
            inner_it = amxc_llist_it_get_next(inner_it);

            if(NULL == rule) {
                continue;
            }

            table = (get_old_table_chain ? fw_rule_get_old_table(rule) : fw_rule_get_table(rule));
            chain = (get_old_table_chain ? fw_rule_get_old_chain(rule) : fw_rule_get_chain(rule));

            if(('\0' == table[0]) || ('\0' == chain[0])) {
                //This rule has invalid data, but maybe it must be deleted.
                if(FW_RULE_FLAG_DELETED == fw_rule_get_flag(rule)) {
                    fw_rule_delete(&rule);
                }
                continue;
            }

            //Set the current table - chain combo.
            if(('\0' == current_table[0]) && ('\0' == current_chain[0])) {
                strncpy(current_table, table, sizeof(current_table) - 1);
                strncpy(current_chain, chain, sizeof(current_chain) - 1);
            } else {

                //Still processing a previous table - chain combo. Verify current rule.
                if(strcmp(current_chain, chain) ||
                   strcmp(current_table, table)) {

                    if((NULL == next_chain_it) && (traversed != fw_rule_get_traversed(rule))) {
                        next_chain_it = fw_rule_get_global_list_iterator((fw_rule_t*) rule);
                    }

                    /*
                     * The current rule is for a different table and/or chain. Skip it for now.
                     * The rule is processed in the next chain iteration.
                     */
                    continue;
                }
            }

            fw_rule_set_traversed(rule, traversed);
            index++;

            flag = fw_rule_get_flag(rule);

            //For processed rules, only the incremented index is of importance.
            if(FW_RULE_FLAG_HANDLED == flag) {
                continue;
            }

            if(traversed == FW_TRAVERSED_DEL) {

                if(!((FW_RULE_FLAG_MODIFIED == flag) || (FW_RULE_FLAG_DELETED == flag))) {
                    if(!fw_rule_is_enabled(rule)) {
                        index--;
                    }
                    continue;
                }

                /*
                 * Even when a rule is modified, we first delete it. In the second loop,
                 * the rule is added again.
                 */
                if(cb(rule, FW_RULE_FLAG_DELETED, chain, table, index) == true) {
                    index--;
                } else {
                    callback_failed = true;
                }

                if(FW_RULE_FLAG_DELETED == flag) {
                    fw_rule_delete(&rule);
                }

            } else if(traversed == FW_TRAVERSED_ADD) {

                if(!((FW_RULE_FLAG_MODIFIED == flag) || (FW_RULE_FLAG_NEW == flag))) {
                    continue;
                }

                if(!fw_rule_is_enabled(rule)) {
                    index--;
                } else if(cb(rule, flag, chain, table, index) == false) {
                    index--;
                    callback_failed = true;
                } else {
                    fw_rule_set_flag(rule, FW_RULE_FLAG_HANDLED);
                }

                /*
                 * Set the old table and old chain values. These are used to determine which rule must
                 * be deleted when one is altered.
                 */
                fw_rule_set_old_chain(rule, chain);
                fw_rule_set_old_table(rule, table);
            }
        }

    }

    if(callback_failed) {
        retval = FW_COMMIT_CB_ERROR;
    } else {
        retval = FW_COMMIT_OK;
    }

exit:
    return retval;
}
