/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <netinet/in.h>
#include <fwrules/fw_rule.h>

static void print_rule(fw_rule_t* rule) {
    const char* chain = NULL;
    const char* table = NULL;
    bool ipv6 = false;
    uint32_t protocol = 0;
    uint32_t dest_port = 0;
    bool enabled = false;
    fw_rule_target_t target = FW_RULE_TARGET_LAST;
    fw_rule_policy_t policy = FW_RULE_POLICY_LAST;

    if(!rule) {
        return;
    }

    chain = fw_rule_get_chain(rule);
    table = fw_rule_get_table(rule);
    ipv6 = fw_rule_get_ipv6(rule);
    protocol = fw_rule_get_protocol(rule);
    dest_port = fw_rule_get_destination_port(rule);
    enabled = fw_rule_is_enabled(rule);

    target = fw_rule_get_target(rule);
    policy = fw_rule_get_target_policy_option(rule);

    printf("Rule parameters:\n");
    printf("\tChain: %s\n", chain);
    printf("\tTable: %s\n", table);
    printf("\tIPv6: %s\n", (ipv6 ? "yes" : "no"));
    printf("\tProtocol: %u\n", protocol);
    printf("\tDestination port: %u\n", dest_port);
    printf("\tEnabled: %s\n", (enabled ? "yes" : "no"));
    printf("\tTarget: %s\n", (target == FW_RULE_TARGET_POLICY ? "policy" : "error"));
    printf("\tPolicy: %s\n", (policy == FW_RULE_POLICY_REJECT ? "reject" : "error"));
}

int main(int argc, const char* argv[]) {
    fw_rule_t* rule = NULL;
    int retval = 0;

    retval = fw_rule_new(&rule);

    if(retval) {
        printf("Failed to create rule\n");
        return 1;
    }

    retval |= fw_rule_set_chain(rule, "FORWARD");
    retval |= fw_rule_set_table(rule, "filter");
    retval |= fw_rule_set_ipv6(rule, true);
    retval |= fw_rule_set_protocol(rule, IPPROTO_UDP);
    retval |= fw_rule_set_destination_port(rule, 80);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_REJECT);

    //Don't forget to enable the rule!
    retval |= fw_rule_set_enabled(rule, true);

    if(retval) {
        printf("Failed to apply parameters\n");
        goto exit;
    }

    print_rule(rule);

exit:

    retval = fw_rule_delete(&rule);

    if(retval) {
        printf("Failed to delete rule\n");
        return 1;
    }

    return 0;
}
